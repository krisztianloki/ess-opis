<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(DEVICENAME)</name>
  <width>751</width>
  <height>360</height>
  <widget type="rectangle" version="2.0.0">
    <name>Gauge Background</name>
    <width>751</width>
    <height>360</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Gauge</name>
    <text>$(DEVICENAME)</text>
    <width>751</width>
    <height>35</height>
    <font>
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Status</name>
    <text>XX @ Yy</text>
    <x>671</x>
    <y>5</y>
    <width>70</width>
    <height>25</height>
    <font>
      <font name="SMALL-MONO-BOLD" family="Source Code Pro" style="BOLD" size="14.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <scripts>
      <script file="../../COMMON/Faceplate/gauge_status.py">
        <pv_name>$(DEVICENAME):ValidR</pv_name>
        <pv_name>$(DEVICENAME):SensorTypeR</pv_name>
        <pv_name>$(DEVICENAME):ChanR</pv_name>
      </script>
    </scripts>
    <tooltip>NA</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Flow</name>
    <x>10</x>
    <y>55</y>
    <width>385</width>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Pressure Background</name>
      <width>385</width>
      <height>200</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Flow Header</name>
      <text>FLOW</text>
      <width>385</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Flow</name>
      <text>Flow:</text>
      <y>40</y>
      <width>120</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>FlowR</name>
      <pv_name>$(DEVICENAME):FlwR</pv_name>
      <x>130</x>
      <y>40</y>
      <width>120</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <format>2</format>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>FlowStatR</name>
      <pv_name>$(DEVICENAME):FlwStatR</pv_name>
      <x>260</x>
      <y>40</y>
      <width>115</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <scripts>
        <script file="../../COMMON/Faceplate/pressure_status.py">
          <pv_name>$(DEVICENAME):FlwStatR</pv_name>
          <pv_name>$(DEVICENAME):FlwR-STR</pv_name>
        </script>
      </scripts>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Flow Setpoint</name>
      <text>Flow Setpoint:</text>
      <y>95</y>
      <width>120</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>FlowSetpointS</name>
      <pv_name>$(DEVICENAME):FlwSPS</pv_name>
      <x>130</x>
      <y>95</y>
      <width>120</width>
      <height>25</height>
      <format>2</format>
      <precision>2</precision>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>FlowSetpointR</name>
      <pv_name>$(DEVICENAME):FlwSP-RB</pv_name>
      <x>256</x>
      <y>95</y>
      <width>119</width>
      <height>25</height>
      <format>2</format>
      <precision>2</precision>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Scale Factor</name>
      <text>Scale Factor:</text>
      <y>130</y>
      <width>120</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>ScaleFactorS</name>
      <pv_name>$(DEVICENAME):ScaleFS</pv_name>
      <x>130</x>
      <y>130</y>
      <width>120</width>
      <height>25</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>ScaleFactor-RB</name>
      <pv_name>$(DEVICENAME):ScaleF-RB</pv_name>
      <x>256</x>
      <y>130</y>
      <width>119</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Nominal Range</name>
      <text>Nominal Range:</text>
      <y>165</y>
      <width>120</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Full Scale Nominal Range</tooltip>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>NominalRangeS</name>
      <pv_name>$(DEVICENAME):NomRngS</pv_name>
      <x>130</x>
      <y>165</y>
      <width>120</width>
      <height>25</height>
      <format>2</format>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>NominalRange-RB</name>
      <pv_name>$(DEVICENAME):NomRng-RB</pv_name>
      <x>256</x>
      <y>165</y>
      <width>119</width>
      <height>25</height>
      <format>2</format>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Misc</name>
    <x>415</x>
    <y>55</y>
    <width>326</width>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Misc Background</name>
      <width>326</width>
      <height>200</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Misc Header</name>
      <text>MISC</text>
      <width>326</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Mode</name>
      <text>Mode:</text>
      <y>40</y>
      <width>80</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>ModeS</name>
      <pv_name>$(DEVICENAME):ModeS</pv_name>
      <x>90</x>
      <y>40</y>
      <width>110</width>
      <height>25</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Mode-RB</name>
      <pv_name>$(DEVICENAME):Mode-RB</pv_name>
      <x>206</x>
      <y>40</y>
      <width>110</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>$(pv_name)$(pv_value)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Zero Gague</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Zero the Gauge</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):ZeroS</pv_name>
      <x>166</x>
      <y>160</y>
      <width>150</width>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Separator</name>
    <x>5</x>
    <y>275</y>
    <width>741</width>
    <height>3</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Controller Detailed</name>
    <actions>
      <action type="open_display">
        <file>../../veg/Faceplate/vac_ctrl_mks946_937b_controller.bob</file>
        <macros>
          <DEVICENAME>$(CONTROLLER)</DEVICENAME>
        </macros>
        <target>standalone</target>
        <description>Open Controller Screen</description>
      </action>
    </actions>
    <x>581</x>
    <y>293</y>
    <width>150</width>
    <height>50</height>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
