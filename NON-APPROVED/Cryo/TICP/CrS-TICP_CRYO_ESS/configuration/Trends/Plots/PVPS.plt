<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title>PVPS</title>
  <show_legend>true</show_legend>
  <show_toolbar>true</show_toolbar>
  <grid>true</grid>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-12 hours</start>
  <end>now</end>
  <archive_rescale>STAGGER</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>255</red>
    <green>255</green>
    <blue>255</blue>
  </background>
  <title_font>Liberation Sans|20|1</title_font>
  <label_font>Liberation Sans|14|1</label_font>
  <scale_font>Liberation Sans|12|0</scale_font>
  <legend_font>Liberation Sans|14|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>mbar</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>-6590.0</min>
      <max>1190.0</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>true</visible>
      <name>g/s</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>-61.800000000000004</min>
      <max>32.6</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>false</visible>
      <name>bar</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>835.0</min>
      <max>1298.0</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>true</visible>
      <name>ppm</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>true</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>244.32005694939184</min>
      <max>255.07331654389583</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>true</visible>
      <name>%</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>true</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>-130.0</min>
      <max>590.0</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>false</visible>
      <name>degC</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>false</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>-5.0</min>
      <max>805.0</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>PT-23920</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-PT-23920:Val</name>
      <axis>2</axis>
      <color>
        <red>128</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>PT-24900A</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-PT-24900A:Val</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>PT-24900B</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-PT-24900B:Val</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>183</green>
        <blue>171</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>PT-24911</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-PT-24911:Val</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>160</green>
        <blue>0</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>PT-24912</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-PT-24912:Val</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>219</green>
        <blue>0</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>PT-24913</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-PT-24913:Val</name>
      <axis>0</axis>
      <color>
        <red>255</red>
        <green>255</green>
        <blue>85</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-23920</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-23920:Val</name>
      <axis>5</axis>
      <color>
        <red>36</red>
        <green>120</green>
        <blue>0</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-24911</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-24911:Val</name>
      <axis>5</axis>
      <color>
        <red>66</red>
        <green>181</green>
        <blue>14</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-24912</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-24912:Val</name>
      <axis>5</axis>
      <color>
        <red>80</red>
        <green>254</green>
        <blue>67</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-24913</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-24913:Val</name>
      <axis>5</axis>
      <color>
        <red>182</red>
        <green>249</green>
        <blue>169</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-24920A</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-24920A:Val</name>
      <axis>5</axis>
      <color>
        <red>120</red>
        <green>161</green>
        <blue>242</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-24920B</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-24920B:Val</name>
      <axis>5</axis>
      <color>
        <red>0</red>
        <green>36</green>
        <blue>255</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>TT-24920C</display_name>
      <visible>false</visible>
      <name>CrS-TICP:Cryo-TT-24920C:Val</name>
      <axis>5</axis>
      <color>
        <red>2</red>
        <green>15</green>
        <blue>135</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>CV-23920B</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-CV-23920B:Out</name>
      <axis>4</axis>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>CV-23920A</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-CV-23920A:Out</name>
      <axis>4</axis>
      <color>
        <red>0</red>
        <green>128</green>
        <blue>0</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>FT-23920</display_name>
      <visible>true</visible>
      <name>CrS-TICP:Cryo-FT-23920:Val</name>
      <axis>1</axis>
      <color>
        <red>255</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</name>
        <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
  </pvlist>
</databrowser>
