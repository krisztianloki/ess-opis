<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>ODH Main</name>
  <macros>
    <EMBEDDED_DISPLAYS>99-Shared/embedded_displays</EMBEDDED_DISPLAYS>
    <POPUPS>99-Shared/popups</POPUPS>
    <TS2_PATH>../TS2/pss</TS2_PATH>
  </macros>
  <width>645</width>
  <height>950</height>
  <widget type="group" version="2.0.0">
    <name>group.HeCompBuilding</name>
    <macros>
      <P>HCB-ACH:ODH-O2iM</P>
    </macros>
    <x>10</x>
    <y>10</y>
    <width>620</width>
    <height>930</height>
    <style>3</style>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <widget type="rectangle" version="2.0.0">
      <name>group.HeCompBuilding.border</name>
      <width>620</width>
      <height>930</height>
      <line_width>0</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </background_color>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>PLC Button</name>
      <actions>
        <action type="open_display">
          <file>$(POPUPS)/ODH_Expert_Diag.bob</file>
          <target>tab</target>
          <description>PLC Diagnostic</description>
        </action>
      </actions>
      <x>12</x>
      <y>10</y>
      <width>130</width>
      <height>40</height>
      <background_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </background_color>
      <rules>
        <rule name="Color" prop_id="background_color" out_exp="false">
          <exp bool_exp="pvInt0">
            <value>
              <color name="Button_Background" red="236" green="236" blue="236">
              </color>
            </value>
          </exp>
          <pv_name>KG-GTA:ODH-PLC-01:CPU_Status</pv_name>
        </rule>
      </rules>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="led" version="2.0.0">
      <name>PLC LED</name>
      <pv_name>KG-GTA:ODH-PLC-01:AliveR</pv_name>
      <x>162</x>
      <y>10</y>
      <width>161</width>
      <height>40</height>
      <off_label>Lost PLC Connection</off_label>
      <off_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </off_color>
      <on_label>PLC Connection</on_label>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <square>true</square>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Rack Button</name>
      <actions>
        <action type="open_display">
          <file>$(POPUPS)/ODH_Expert_Racks.bob</file>
          <target>standalone</target>
          <description>Electrical Racks</description>
        </action>
      </actions>
      <x>12</x>
      <y>68</y>
      <width>130</width>
      <height>25</height>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>O2 Level</name>
      <text>O2 Level</text>
      <x>172</x>
      <y>70</y>
      <width>61</width>
      <height>25</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <auto_size>true</auto_size>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="label" version="2.0.0">
      <name>ODH Warning</name>
      <text>ODH 19.5%
Warning</text>
      <x>254</x>
      <y>60</y>
      <width>80</width>
      <height>49</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <auto_size>true</auto_size>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="label" version="2.0.0">
      <name>ODH Alarm</name>
      <text>ODH 18%
Alarm</text>
      <x>347</x>
      <y>60</y>
      <width>66</width>
      <height>49</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <auto_size>true</auto_size>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Monitor Healthy</name>
      <text>Monitor
Healthy</text>
      <x>437</x>
      <y>60</y>
      <width>57</width>
      <height>49</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <auto_size>true</auto_size>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Maintenance</name>
      <text>Maintenance</text>
      <x>513</x>
      <y>70</y>
      <width>92</width>
      <height>25</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <auto_size>true</auto_size>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>HCB Button</name>
      <actions>
        <action type="open_display">
          <file>ODH_Area_Layout_HCB.bob</file>
          <macros>
            <LOC>Helium Compressor Building (HCB) &amp; Dogshed</LOC>
            <LOC_ACRONYM>HCB &amp; Dogshed</LOC_ACRONYM>
            <P>HCB-ACH:ODH-Area</P>
          </macros>
          <target>tab</target>
          <description>Helium Compressor Building (HCB) &amp; Dogshed</description>
        </action>
      </actions>
      <x>12</x>
      <y>111</y>
      <width>50</width>
      <height>381</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group HCB</name>
      <macros>
        <AREA>HCB</AREA>
      </macros>
      <x>72</x>
      <y>111</y>
      <width>535</width>
      <height>380</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>group.HeCompBuilding.background</name>
        <width>535</width>
        <height>380</height>
        <line_width>0</line_width>
        <line_color>
          <color name="PRIMARY-GROUP-BORDER" red="151" green="188" blue="202">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>1</IDX>
          <P>HCB-ACH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>5</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>2</IDX>
          <P>HCB-ACH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>37</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>3</IDX>
          <P>HCB-ACH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>68</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>4</IDX>
          <P>HCB-ACH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>99</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>5</IDX>
          <P>HCB-ACH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>130</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>6</IDX>
          <P>HCB-TCH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>161</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>7</IDX>
          <P>HCB-TCH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>192</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>8</IDX>
          <P>HCB-TCH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>223</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>9</IDX>
          <P>HCB-TCH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>254</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>10</IDX>
          <P>HCB-HPGS:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>285</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>11</IDX>
          <P>HCB-HPGS:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>316</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>12</IDX>
          <P>HCB-DogSh:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>347</y>
        <width>510</width>
        <height>30</height>
      </widget>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>CXH Button</name>
      <actions>
        <action type="open_display">
          <file>ODH_Area_Layout_CXB.bob</file>
          <macros>
            <LOC>ColdBox Hall (CXH)</LOC>
            <LOC_ACRONYM>CXH</LOC_ACRONYM>
            <P>CXB-CXH:ODH-Area</P>
          </macros>
          <target>tab</target>
          <description>ColdBox Hall (CXH)</description>
        </action>
      </actions>
      <x>12</x>
      <y>497</y>
      <width>50</width>
      <height>227</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group CXH</name>
      <macros>
        <AREA>CXH</AREA>
      </macros>
      <x>72</x>
      <y>497</y>
      <width>535</width>
      <height>227</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>group.HeCompBuilding.background_2</name>
        <width>535</width>
        <height>227</height>
        <line_width>0</line_width>
        <line_color>
          <color name="PRIMARY-GROUP-BORDER" red="151" green="188" blue="202">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>13</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>6</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>14</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>38</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>15</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>69</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>16</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>100</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>17</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>131</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>18</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>162</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>19</IDX>
          <P>CXB-CXH:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>193</y>
        <width>510</width>
        <height>30</height>
      </widget>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>CTL Button</name>
      <actions>
        <action type="open_display">
          <file>ODH_Area_Layout_CTL.bob</file>
          <macros>
            <LOC>CTL Gallery (CTLG)</LOC>
            <LOC_ACRONYM>CTLG</LOC_ACRONYM>
            <P>CTLG-CS:ODH-Area</P>
          </macros>
          <target>tab</target>
          <description>CTL Gallery (CTLG)</description>
        </action>
      </actions>
      <x>12</x>
      <y>730</y>
      <width>50</width>
      <height>105</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group CTL</name>
      <macros>
        <AREA>CTL</AREA>
      </macros>
      <x>72</x>
      <y>730</y>
      <width>535</width>
      <height>105</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>group.HeCompBuilding.background_3</name>
        <width>535</width>
        <height>105</height>
        <line_width>0</line_width>
        <line_color>
          <color name="PRIMARY-GROUP-BORDER" red="151" green="188" blue="202">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>20</IDX>
          <P>CTLG-CS:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>6</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>21</IDX>
          <P>CTLG-CS:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>38</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>22</IDX>
          <P>CTLG-CS:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>69</y>
        <width>510</width>
        <height>30</height>
      </widget>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>TS2 Button</name>
      <actions>
        <action type="open_display">
          <file>ODH_Area_Layout_TS2.bob</file>
          <macros>
            <LOC>TS2 bunker</LOC>
            <LOC_ACRONYM>TS2</LOC_ACRONYM>
            <P>KG-GTA:PSS-Area-TS2</P>
          </macros>
          <target>standalone</target>
          <description>TS2 bunker</description>
        </action>
      </actions>
      <x>12</x>
      <y>841</y>
      <width>50</width>
      <height>75</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Group TS2</name>
      <macros>
        <AREA>TS2</AREA>
      </macros>
      <x>72</x>
      <y>841</y>
      <width>535</width>
      <height>75</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>group.HeCompBuilding.background_4</name>
        <width>535</width>
        <height>75</height>
        <line_width>0</line_width>
        <line_color>
          <color name="PRIMARY-GROUP-BORDER" red="151" green="188" blue="202">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>1</IDX>
          <P>KG-GTA:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/TS2_ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>6</y>
        <width>510</width>
        <height>30</height>
      </widget>
      <widget type="embedded" version="2.0.0">
        <name>$(P)</name>
        <macros>
          <IDX>2</IDX>
          <P>KG-GTA:ODH-O2iM-$(IDX)</P>
          <R>$(IDX)</R>
        </macros>
        <file>$(EMBEDDED_DISPLAYS)/TS2_ODH_Monit_Sub.bob</file>
        <x>10</x>
        <y>38</y>
        <width>510</width>
        <height>30</height>
      </widget>
    </widget>
  </widget>
</display>
